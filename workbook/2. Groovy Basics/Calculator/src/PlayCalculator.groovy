class PlayCalculator {
    static void main(String[] args) {
        Calculator newOperations = new Calculator()
//        newOperations.operationName = System.console().readLine() - Error code
//        newOperations.firstNumber = System.in.newReader().readLine() as Integer - Error code
//        newOperations.secondNumber = System.in.newReader().readLine() as Integer - Error code
        println "Please enter an operation (+, -, *, /): "
        String operatorName = System.in.newReader().readLine()
        println "Please enter the first number: "
        def x = System.in.newReader().readLine() as Integer
        println "Please enter the second number: "
        def y = System.in.newReader().readLine() as Integer
        newOperations.setOperationName(operatorName)
        newOperations.setFirstNumber(x)
        newOperations.setSecondNumber(y)

        println "Result: ${newOperations.getResult()}"
    }
}