class Calculator {
    def operationName
    def firstNumber
    def secondNumber
    def getResult() {
        switch(operationName) {
            case "/":
                try {
                    return firstNumber/secondNumber
                } catch(ArithmeticException e) {
                    println "can't be divided by zero"
                }
            case "-":
                println "substraction result is: "
                return firstNumber - secondNumber
            case "+":
                println "substraction result is: "
                return firstNumber + secondNumber
            case "*":
                println "substraction result is: "
                return firstNumber * secondNumber
            default:
                println "Not defined operation"
        }
    }
}
