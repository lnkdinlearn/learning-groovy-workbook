class HelloWorldVar {
    static void main(String[] args) {
        println "Hello World"

        int age = 40

        println age

        println age.getClass()

        def name = "John"

        println name

        println name.getClass()
    }
}
