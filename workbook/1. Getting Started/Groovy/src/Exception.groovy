class Exception {
    static void main(String[] args) {
        // Create new Person class and instantiate it
        Person johnDoe = new Person()
        johnDoe.setFirstName("John")
        johnDoe.setLastName("Doe")
        johnDoe.setAge(40)

        // Convert a String into a Long data type
        //johnDoe.getFirstName().toLong()

        // Catch unchecked exception and handle it by printing a message
        try {
            // Convert a String into a Long data type
            johnDoe.getFirstName().toLong()
        } catch (NumberFormatException e) {
            assert e instanceof NumberFormatException
            println "Cannot convert a String into a Long"
        }

    }
}
