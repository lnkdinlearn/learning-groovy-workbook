class Conditionals {
    static void main(String[] args) {
        // Create new Person class and instantiate it
        Person johnDoe = new Person()
        johnDoe.setFirstName("John")
        johnDoe.setLastName("Doe")
        johnDoe.setAge(40)

        // Print the full name and age of the Person instance
        println johnDoe.getFullName()
        println johnDoe.getAge()

        // Identify if Person is middle-aged using a conditional
        if (johnDoe.getAge() >= 45 && johnDoe.getAge() <=65 ) {
            println johnDoe.getFullName() + " is middle-aged"
        } else {
            println johnDoe.getFullName() + " is " + johnDoe.getAge() + " years old"
        }

        // Define a list of Person
        def persons= [johnDoe, new Person(firstName: "Mary", lastName: "Hill", age: 40)]

        // Iterate over Person instances in list
        for (Person p: persons) {
            println p.getFullName()
        }

    }
}
